module.exports = {
	theme: {
		colors: {
			blue: '#293894',
			orange: '#E94235',
			gray: {
				100: '#CFD1C7',
				200: '#6B6B65'
			},
			white: '#ffffff'
		},
		extend: {
			colors: {
				dark: '#1b1b1b'
			}
		}
	},
	variants: {},
	plugins: [],
	purge: false
}
