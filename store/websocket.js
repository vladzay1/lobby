// Mock of websocket subscribe
export const state = () => ({
	subscriptions: {}
})

export const getters = {
	getSettingsData: state => state.settingsData
}

export const mutations = {
	SUBSCRIBE(state, { key, fn }) {
		const isEmpty = !Object.keys(state.subscriptions).includes(key)

		if (isEmpty) {
			state.subscriptions[key] = fn
		}
	},
	UNSUBSCRIBE(state, key) {
		delete state.subscriptions[key]
	},
	ON_MESSAGE(state, message) {
		Object.values(state.subscriptions).forEach(fn => fn(message))
	}
}

export const actions = {
	subscribe({ commit }, payload) {
		commit('SUBSCRIBE', payload)
	},
	unsubscribe({ commit }, payload) {
		commit('UNSUBSCRIBE', payload)
	},
	init({ commit }) {
		const randomSeconds = Math.floor(Math.random() * 15) * 1000

		setTimeout(() => {
			commit('ON_MESSAGE', { status: 'found' })
		}, randomSeconds)
	}
}
