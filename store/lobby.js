export const state = () => ({
	settingsData: {}
})

export const getters = {
	getSettingsData: state => state.settingsData
}

export const mutations = {
	CHANGE_SETTING(state, { key, value }) {
		state.settingsData = {
			...state.settingsData,
			[key]: value
		}
	}
}

export const actions = {
	changeSetting({ commit }, payload) {
		commit('CHANGE_SETTING', payload)
	}
}
