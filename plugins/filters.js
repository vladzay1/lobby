import Vue from 'vue'

Vue.filter('formatDate', value => {
	const date = new Date(value * 1000)

	return date.toLocaleString(['en-US'], {
		minute: '2-digit',
		second: '2-digit'
	})
})
