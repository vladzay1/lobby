module.exports = {
	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
	},

	loading: { color: '#fff' },

	css: ['~assets/scss/style.scss'],

	plugins: ['~plugins/filters.js'],

	buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/tailwindcss'],

	styleResources: {
		scss: ['~assets/scss/base/_variables.scss']
	},

	modules: ['@nuxtjs/axios', '@nuxtjs/style-resources'],

	axios: {},

	components: true,

	build: {
		extend: config => {
			const svgRule = config.module.rules.find(rule =>
				rule.test.test('.svg')
			)

			svgRule.test = /\.(png|jpe?g|gif|webp)$/

			config.module.rules.push({
				test: /\.svg$/,
				use: ['babel-loader', 'vue-svg-loader']
			})
		}
	}
}
